library "cicd@master"

def user 
pipeline {
    agent {
        node {
            label 'jnlp-slave'
            customWorkspace  "${env.WORKSPACE}/code"
        }
    }
    options {
            skipDefaultCheckout()  //删除隐式checkout scm语句
            disableConcurrentBuilds() //禁止并行
            timeout(time: 1, unit: 'HOURS')  //流水线超时设置1h
    }
    parameters {
        string(
            description: '项目',
            name: 'PROJECT',
            defaultValue: "ms"
        )
        choice(
            description: '你需要选择哪个模块进行构建 ?',
            name: 'SERVICE',
            choices: ['flask-ui']
        )
        choice(
            description: '构建和部署，仅进行构建测试，仅部署,选择发布之后的动作',
            name: 'CICD',
            choices: ['cicd','ci','cd']
        )
        string (
            name: 'CODE_URL',
            defaultValue: 'https://gitee.com/cmlfxz/flask-ui.git',
            description: 'code git  url'
        )
        string(
            description: '副本数',
            name: 'REPLICAS',
            defaultValue: "1"
        )
    }
    environment {
        ENV = 'test'
        CLI = "/usr/bin/kubectl --kubeconfig /root/.kube/config"
        HARBOR_REGISTRY = 'myhub.mydocker.com'
        HARBOR_EMAIL = '915613275@qq.com'
        DOCKER_HUB_ID = 'test-dockerHub'
        GIT_CRED_ID = 'gitee_account'
        BRANCH = "${env.BRANCH_NAME}"
        WORKSPACE = "${env.WORKSPACE}/.."
    }
    stages {
        stage('init') {
            steps{
                script {
                    wrap([$class: 'BuildUser']) {
                        user = env.BUILD_USER
                    }
                }
            }
        }
        stage('checkout code') {
            steps {
                script {
                    revision = 'develop'
                }
                pullCode([git_url:"${params.CODE_URL}",revision:"${revision}",cred_id:"${GIT_CRED_ID}" ])
            }
        }
        stage("get tag") {
            steps {
                script {
                    env.TAG = sh(returnStdout: true, script: 'git rev-parse --short HEAD')
                }
            }
        }
        stage('display var') {
            steps {
                echo "$user  tag:$TAG replicas: ${params.REPLICAS}  ${env.WORKSPACE}"
            }
        }
        stage('build') {
            when {
                expression { return params.CICD!="cd" }
            }
            steps {
                withCredentials([usernamePassword(credentialsId: "$DOCKER_HUB_ID", passwordVariable: 'dockerHubPassword', usernameVariable: 'dockerHubUser')]){
                    sh '''
                        docker login -u ${dockerHubUser} -p ${dockerHubPassword} $HARBOR_REGISTRY
                        cd $WORKSPACE/code
                        /usr/local/bin/npm install --registry=https://registry.npm.taobao.org || true
                        /usr/local/bin/npm config set sass_binary_site=https://npm.taobao.org/mirrors/node-sass
                        /usr/local/bin/npm run test
                        cd $WORKSPACE/cicd/
                        sh build.sh --action=build --env=$ENV --project=$PROJECT --service=$SERVICE --branch=$BRANCH --tag=$TAG  --workspace=$WORKSPACE --harbor_registry=$HARBOR_REGISTRY
                    '''
                }

            }
        }
        stage('deploy test'){
            when {
                expression { return params.CICD!="ci" }
            }
            steps {
                withCredentials([usernamePassword(credentialsId: "$DOCKER_HUB_ID", passwordVariable: 'dockerHubPassword', usernameVariable: 'dockerHubUser')]){
                    configFileProvider([configFile(fileId: 'test-k8s-config', targetLocation: '/root/.kube/config')]) {
                        sh '''
                            namespace="$PROJECT-$ENV"
                            $CLI create namespace $namespace || true
                            $CLI create secret docker-registry harborsecret --docker-server=$HARBOR_REGISTRY --docker-username=$dockerHubUser \
                                --docker-password=$dockerHubPassword --docker-email=$HARBOR_EMAIL --namespace=$namespace || true
                        '''
                    }
                    sh  '''
                        cd $WORKSPACE/cicd/
                        sh  build.sh --action=deploy --env=$ENV --project=$PROJECT --service=$SERVICE --branch=$BRANCH --tag=$TAG  --workspace=$WORKSPACE \
                                     --replicas=$REPLICAS --harbor_registry=$HARBOR_REGISTRY 
                    '''
                }

            }
        }

    }
    post {
        always {
            notify([result: "${currentBuild.result}",user: "$user"])
        }
    }
}
