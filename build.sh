#!/bin/bash
COMMANDLINE="$*"
action=""
env=""
project=""
service=""
tag=""
replicas=""
harbor_registry=""
type=""

for COMMAND in $COMMANDLINE
do
    key=$(echo $COMMAND | awk -F"=" '{print $1}')
    val=$(echo $COMMAND | awk -F"=" '{print $2}')
    case $key in
        --action)
            action=$val
        ;;
        --env)
            env=$val
        ;;
        --project)
            project=$val
        ;;
        --service)
            service=$val
        ;;
        --tag)
            tag=$val
        ;;
        --replicas)
            replicas=$val
        ;;
        --harbor_registry)
            harbor_registry=$val
        ;;
        --type)
            type=$val
        ;;
        --branch)
            branch=$val
        ;;
        --workspace)
            workspace=$val
        ;;
    esac
done

echo "$action $env $project $service $tag $replicas $harbor_registry $type $branch "

if [ "$#" -lt 3 ];then
    echo "缺少参数"
    echo "Usage sh build.sh --action=build/deploy/rollout \
                            --env=dev/test/prod \
                            --project=ms \
                            --service=flask-k8s \
                            --tag=commit_id/v1.0 \
                            --replicas=1 \
                            --harbor_registry=myhub.mydocker.com \
                            --type=ab|canary \
                            --branch=develop
                            --workspace=/home/.jenkins/workspace/job_name"
    exit 1
fi
if [ -z "$replicas" ];then
  replicas=1
  echo "副本数没设置，默认为1"
fi
if [ -z "$harbor_registry" -a `echo $service|grep -i 'gateway' |wc -l` -eq 0 ];then
  echo "harbor仓库没设置" && exit
fi
namespace=$project-$env
code_dir="${workspace}/code"
yaml_dir="${workspace}/cicd/${project}/${service}/$env/yaml"

echo "$code_dir $yaml_dir"
CLI="/usr/bin/kubectl --kubeconfig /root/.kube/config"

HELM="/usr/bin/helm  --kubeconfig /root/.kube/k3s" 

build() {
   echo "当前正在构建$env环境"
   cd $code_dir
   image_name=$harbor_registry/$namespace/${service}:$tag
   docker build -t ${image_name} .
   docker push ${image_name} 
   docker rmi ${image_name}
}

deploy_test() {
    echo "当前正在部署开发环境"
    cd $yaml_dir
    deploy_service
    $CLI get pod,svc -n $namespace
}

deploy_service() {
    $CLI create namespace $namespace
    if [ "$env" == 'prod' ];then
         $CLI label namespace $namespace istio-injection=enabled
    fi
    # kustomize edit set image $harbor_registry/$namespace/$service=$harbor_registry/$namespace/${service}:$tag
    # kustomize edit set namespace $namespace
    # if [ "$env" == "prod" ];then
    #     kustomize edit set replicas $service-$tag=$replicas
    # else
    #     kustomize edit set replicas $service=$replicas
    # fi
    # kustomize build . 
    # kustomize build . |$CLI apply -f -
    # $HELM install  --set replicas=$replicas --set image.tag=$tag  $service-$env-$tag .  --dry-run

    # if [ "$?" -ne 0 ];then
    #     $HELM upgrade --set replicas=$replicas --set image.tag=$tag  $service-$env-$tag .
    # fi
    $HELM install  --set replicas=$replicas --set image.tag=$tag  $service-$env . --dry-run | tail -n +9

    $HELM install  --set replicas=$replicas --set image.tag=$tag  $service-$env . --dry-run   |tail -n +9 |$CLI apply -f -

    
    if [ "$env" == "prod" ];then
        $CLI -n $namespace patch deployment $service-${tag} -p "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"date\":\"`date +'%s'`\"}}}}}"
    fi
}



deploy_prod() {
    echo "当前正在部署生产环境"
    case $type in
        'bluegreen'|'gray')
            echo "发布deployment,svc"
            # cd $yaml_dir/$tag
            cd $yaml_dir
            deploy_service
            $CLI get deployment,pod -n $namespace
        ;;
        *)
            echo "没有$type这种发布模式" && exit 1
        ;;
    esac
}

rollout(){
    echo "你正在执行$env环境回滚操作"
    dir="$yaml_dir/rollout"
    [ ! -d "$dir" ] && echo "没有$dir回滚目录，请检查" && exit 1
    cd $dir
    kustomize edit set namespace $namespace
    kustomize build . &&  kustomize build . |$CLI apply -f -
    $CLI get gw,dr,vs,svc  -n $namespace
}
case $action in
    build)
        build
    ;;
    deploy)
        deploy_$env
    ;;
    rollout)
        rollout
    ;;
esac