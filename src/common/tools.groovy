package common


def ColorPrint(value,color){
    
    colors = ['red'   : "\033[47;31m ${value} \033[0m",
              'blue'  : "\033[47;34m ${value} \033[0m",
              'green' : "\033[47;32m ${value} \033[0m" ]

    ansiColor('xterm') {
        println(value+color)
        println(colors[color])
    }
}
return this