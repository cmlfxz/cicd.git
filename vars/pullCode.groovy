def call(params) {
    if(params.dir==''|| params.dir==null){
        echo '空目录'
        params.dir = '.'
    }
    if(params.git_url==''||params.git_url==null){
        echo 'git_url不允许为空'
        return
    }
    checkout([
        $class: 'GitSCM', 
        branches: [[name: "${params.revision}" ]], 
        doGenerateSubmoduleConfigurations: false, 
        extensions: [[$class: 'RelativeTargetDirectory',relativeTargetDir: "${params.dir}"]], 
        submoduleCfg: [], 
        userRemoteConfigs: [[
            credentialsId: "${params.cred_id}", 
            url: "${params.git_url}" 
        ]]
    ])
    
}