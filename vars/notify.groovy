// import common.tools
def dingDing(status,user){
        dingtalk (
            robot: '4def1f0b-4f7c-4793-b1d0-6f5394afa257',
            type: 'ACTION_CARD',
            title: "$JOB_NAME",
            text: [
                "### [${env.JOB_NAME}](${env.JOB_URL}) ",
                '---',
                "- 任务ID：[${currentBuild.displayName}](${env.BUILD_URL})",
                "- 状态：${status} ${currentBuild.result}",
                "- 持续时间：${currentBuild.durationString}".split("and counting")[0],
                "- 执行人：${user}",
            ],
            messageUrl: "${JOB_URL}",
        ) 
}
def call(params) {
    def status 
    sh "echo ${params.result}"
    if(params.result=='SUCCESS'){
        status = '<font color=#00CD00 >成功</font>'
        dingDing(status,params.user)
    }else if(params.result == 'FAILURE'){
        status = '<font color=#EE0000 >失败</font>'
        dingDing(status,params.user)
    }else if(params.result=='ABORTED'){
        status = '<font color=#EE0000 >取消</font>'
        dingDing(status,params.user)
    }else {
        status = '<font color=#EE0000 >其他</font>'
        dingDing(status,params.user)
    }
}
