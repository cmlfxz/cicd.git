// import common.tools
def call(params) {
    // def tools = new tools()
    // tools.ColorPrint("${params.language}",'blue')
    withSonarQubeEnv('sonar') {
        switch (params.language) {
            case "java":
                sh "mvn clean verify sonar:sonar   -Dmaven.test.skip=true -Dsonar.projectKey=${params.projectKey} "
                break
            case "python":
                def scannerHome = tool 'sonar_scanner'
                sh """
                    echo python
                    ${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=${params.projectKey}  -Dsonar.sources=${params.sources}  
                """
                break
            default:
                sh """
                    echo  ${params.language} 
                    echo  default
                """
        }
    }
}
